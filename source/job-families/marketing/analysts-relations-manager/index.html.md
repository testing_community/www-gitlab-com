---
layout: job_family_page
title: "Analyst Relations Manager"
---

## Analyst Relations Manager

As a Global Industry Analyst and Influencer Relations Manager, you will be part of a team managing all industry analyst, influencer, and thought leadership relationships for GitLab. 

### Requirements

- Prior experience working with industry analysts and influencers in B2B enterprise software and services required.
- Specific, significant experience in an industry Analyst Relations role in enterprise software a plus.
- Understanding of how to build and maintain relationships with key influencers, leading industry analysts, and analyst firms.
- Prior, significant experience working with senior executives a plus.
- Prior experience negotiating contracts with Industry Analyst firms a plus.
- Excellent project and time management skills.
- Excellent written and oral communications skills.
- Self-starter with a strong sense of ownership.
- Able to prioritize in a complex, fast-paced and lean organization.
- Passion for helping build a world-class, innovative analyst relations program and desire to own and refine key operational processes 
- You share our [values](/handbook/values/), and work in accordance with those values

### Responsibilities

- Help design, execute, and manage GitLab's overall industry analyst, influencer, and thought leader strategy and plan. 
- Create, nurture, and manage positive relationships with industry analysts, industry influencers, and thought leaders, serving as a key, centralized contact point and connector.
- Establish and maintain regular communication with analysts and influencers via multiple channels which you will develop to engage and educate these communities on GitLab strategy, roadmap and product updates.
- Ensure all analysts and influencer advice informs all applicable business activities and that analyst/influencer/thought leader publications and/or other interactions mentioning GitLab are leveraged appropriately to help build market awareness, increase lead generation effectiveness, and simplify selling.
- Plan, schedule, and manage analyst/influencer program activities such as participation in analyst research, conferences, advisory days, briefings, inquiries, and reporting.
- Drive alignment with sales, marketing and product teams to maximize program impact.
- Schedule briefings, inquiries, demos and advisory days with key analysts and influencers, GitLab customers, and GitLab executives.
- Prioritize and manage incoming analyst requests for information, research support, research review, customer references, and event speakers.
- Respond to, serve, and manage internal requests for Industry Analyst/Influencer/Thought Leader interactions, publications, research, event support, and other requests as needed.
- Negotiate, secure, and manage contracts and related services ensuring they support both strategic and tactical, near term and future requirements. Ensure that contracted services are fully utilized.
- Support the GitLab reference program, market research activities, and serve as a program manager for the GitLab Thought Leadership Advisory Board.

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 45 minute interview with a Senior Product Marketing Manager
- Then, candidates will be invited to schedule a 45 minute interview with an Analyst Relations Manager
- Candidates will then be invited to schedule a 45 minute interview with our VP of Product or a Director of Product.
- Following that, candidates will be invited to schedule a 45 minute interview with our Manager, Market Research and Customer Insights.
- Then, Candidates will be invited to schedule a 45 minute interview with our Director of Product Marketing.
- Finally, selected candidates may be asked to interview with our CMO and/or CEO.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
