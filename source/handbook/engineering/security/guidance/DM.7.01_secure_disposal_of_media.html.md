---
layout: markdown_page
title: "DM.7.01 - Secure Disposal of Media Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# DM.7.01 - Secure Disposal of Media

## Control Statement

GitLab securely erases media containing decommissioned red and orange data and obtains a certificate or log of erasure; media pending erasure are stored within a secured facility.

## Context

Securely disposing of both electronic and physical media adds a layer of protection from the data being disposed being recovered by unauthorized persons. There are several effective, publicly available tools and techniques to recover data from electronic and physical media, including hard drives and shredded paper. This control aims to reduce the risk of data being recovered by unauthorized persons and shows customers, GitLab team-members, and partners we take measures to protect their data even after it's done being used.

## Scope

This control applies to both electronic and physical (for example, paper printouts) media.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.7.01_secure_disposal_of_media.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.7.01_secure_disposal_of_media.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/DM.7.01_secure_disposal_of_media.md).



## Framework Mapping

* ISO
  * A.11.2.7
  * A.8.3.2
* SOC2 CC
  * CC6.5
* PCI
  * 9.8
  * 9.8.1
  * 9.8.2
